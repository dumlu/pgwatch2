#!/bin/bash

mkdir /var/run/grafana && chown grafana /var/run/grafana

if ! [[ -f /etc/pgwatch2/persistent-config/self-signed-ssl.key ]] || ! [[ -f /etc/pgwatch2/persistent-config/self-signed-ssl.pem ]]; then
    openssl req -x509 -newkey rsa:4096 -keyout /etc/pgwatch2/persistent-config/self-signed-ssl.key -out /etc/pgwatch2/persistent-config/self-signed-ssl.pem -days 3650 -nodes -sha256 -subj '/CN=pw2'
    cp /etc/pgwatch2/persistent-config/self-signed-ssl.pem /etc/ssl/certs/ssl-cert-snakeoil.pem
    cp /etc/pgwatch2/persistent-config/self-signed-ssl.key /etc/ssl/private/ssl-cert-snakeoil.key
    chown postgres /etc/ssl/certs/ssl-cert-snakeoil.pem /etc/ssl/private/ssl-cert-snakeoil.key
    chmod -R 0600 /etc/ssl/certs/ssl-cert-snakeoil.pem /etc/ssl/private/ssl-cert-snakeoil.key
    chmod -R o+rx /etc/pgwatch2/persistent-config
fi

# enable password encryption by default from v1.8.0
if ! [[ -f /etc/pgwatch2/persistent-config/default-password-encryption-key.txt ]]; then
  echo -n "${RANDOM}${RANDOM}${RANDOM}${RANDOM}" > /etc/pgwatch2/persistent-config/default-password-encryption-key.txt
  chmod 0600 /etc/pgwatch2/persistent-config/default-password-encryption-key.txt
fi

GRAFANASSL="${PW2_GRAFANASSL,,}"    # to lowercase
if [[ "$GRAFANASSL" == "1" ]] || [[ "${GRAFANASSL:0:1}" == "t" ]]; then
    if ! grep -q 'protocol = http$' /etc/grafana/grafana.ini; then
        sed -i 's/protocol = http.*/protocol = https/' /etc/grafana/grafana.ini
    fi
fi

if [[ -n "$PW2_GRAFANAUSER" ]]; then
    sed -i "s/admin_user =.*/admin_user = ${PW2_GRAFANAUSER}/" /etc/grafana/grafana.ini
fi

if [[ -n "$PW2_GRAFANAPASSWORD" ]]; then
    sed -i "s/admin_password =.*/admin_password = ${PW2_GRAFANAPASSWORD}/" /etc/grafana/grafana.ini
fi

if [[ -n "$PW2_GRAFANANOANONYMOUS" ]]; then
CFG=$(cat <<-'HERE'
[auth.anonymous]
enabled = false
HERE
)
echo "$CFG" >> /etc/grafana/grafana.ini
fi

if ! [[ -f /etc/pgwatch2/persistent-config/db-bootstrap-done-marker ]]; then
  pg_ctlcluster "${POSTGRES_VERSION}" main start -- --wait

  su -c "psql -d postgres -f /etc/pgwatch2/bootstrap/change_pw.sql" postgres
  su -c "psql -d postgres -f /etc/pgwatch2/bootstrap/grant_monitor_to_pgwatch2.sql" postgres
  su -c "psql -d postgres -f /etc/pgwatch2/bootstrap/create_db_grafana.sql" postgres
  su -c "psql -d postgres -f /etc/pgwatch2/bootstrap/create_db_metric_store.sql" postgres
  su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/00_schema_base.sql" postgres
  su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/01_old_metrics_cleanup_procedure.sql" postgres
  if [[ "$PW2_PG_SCHEMA_TYPE" == "metric-dbname-time" ]]; then
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/metric-dbname-time/metric_store_part_dbname_time.sql" postgres
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/metric-dbname-time/ensure_partition_metric_dbname_time.sql" postgres
  elif [[ "$PW2_PG_SCHEMA_TYPE" == "timescale" ]]; then
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/timescale/ensure_partition_timescale.sql" postgres
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/timescale/change_compression_interval.sql" postgres
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/timescale/metric_store_timescale.sql" postgres
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/timescale/change_chunk_interval.sql" postgres
    if [[ -n "$PW2_TIMESCALE_CHUNK_HOURS" ]]; then
      su -c "psql -d pgwatch2_metrics -c \"SELECT admin.timescale_change_chunk_interval('$PW2_TIMESCALE_CHUNK_HOURS hours'::interval);\"" postgres
    else
      su -c "psql -d pgwatch2_metrics -c \"SELECT admin.timescale_change_chunk_interval('24 hours'::interval);\"" postgres
    fi    
    if [[ -n "$PW2_TIMESCALE_COMPRESS_HOURS" ]]; then
      su -c "psql -d pgwatch2_metrics -c \"SELECT admin.timescale_change_compress_interval('$PW2_TIMESCALE_COMPRESS_HOURS hours'::interval);\"" postgres
    fi
  else
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/metric-time/metric_store_part_time.sql" postgres
    su -c "psql -d pgwatch2_metrics -f /etc/pgwatch2/sql/metric_store/metric-time/ensure_partition_metric_time.sql" postgres
  fi

  if [[ -n "$PW2_TESTDB" ]]; then
    echo "TestDB functionality is not available due to yaml based setup"
    exit 1
  fi

  DBS="postgres pgwatch2_grafana pgwatch2_metrics"
  for db in $DBS; do su -c "psql -d $db -f /etc/pgwatch2/bootstrap/grant_extra_monitoring_to_pgwatch2.sql" postgres; done

  touch /etc/pgwatch2/persistent-config/db-bootstrap-done-marker

  pg_ctlcluster "${POSTGRES_VERSION}" main stop -- --wait
fi

sleep 1

exec /usr/bin/supervisord --configuration=/etc/supervisor/supervisord.conf --nodaemon
