SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  queryid::text as tag_queryid,
  user_time,
  system_time,
  minflts,
  majflts,
  reads,
  writes,
  nvcsws,
  nivcsws
FROM pg_stat_kcache()
WHERE dbid = (select oid from pg_database where datname = current_database())
ORDER BY queryid;
