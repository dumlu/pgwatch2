select /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  quote_ident(nspname) as tag_schema,
  quote_ident(relname) as tag_table_name,
  quote_ident(nspname)||'.'||quote_ident(relname) as tag_table_full_name,
  c.reltuples as row_count
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE 
  nspname NOT IN ('pg_catalog', 'information_schema') AND
  relkind='r';
