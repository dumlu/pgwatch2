# pgwatch2 - Postgres.ai Edition
A modified version of [pgwatch2](https://github.com/cybertec-postgresql/pgwatch2), a popular monitoring tool developed by [Cybertec](https://www.cybertec-postgresql.com/en/).

Modifications:
- Improved dashboards for fast troubleshooting
- Custom metrics and configs
- Support of [pg_wait_sampling](https://github.com/postgrespro/pg_wait_sampling) and [pg_stat_kcache](https://github.com/powa-team/pg_stat_kcache)
- Used [YAML based setup](https://pgwatch2.readthedocs.io/en/latest/custom_installation.html#yaml-based-setup)
- Metrics storage DB: [PostgreSQL](https://www.postgresql.org) with [timescaledb](https://www.timescale.com) extension (by default)

### Demo: https://pgwatch.postgres.ai

- username: `demo`
- password: `demo`

## How to configure and run

First, on Postgres hosts we want to observe:

- DB user that will be used by pgwatch2 + permissions it needs

```sql
CREATE ROLE pgwatch2 WITH LOGIN PASSWORD 'MY_SECRET_PASS';
GRANT pg_monitor TO pgwatch2;
GRANT EXECUTE ON FUNCTION pg_stat_file(text) to pgwatch2;
GRANT EXECUTE ON FUNCTION pg_ls_dir(text) TO pgwatch2;
GRANT EXECUTE ON FUNCTION pg_wait_sampling_reset_profile() TO pgwatch2; --if pg_wait_sampling extension is used
GRANT CONNECT ON DATABASE mydb TO pgwatch2;
GRANT USAGE ON SCHEMA public TO pgwatch2;
```
- Add pgwatch2 host to pg_hba.conf

- Add extensions

For most monitored databases it’s extremely beneficial (to troubleshooting performance issues) to also activate  the pg_stat_statements extension:

```
create extension if not exists pg_stat_statements;
```
and optional (can be omitted):
```
sudo apt-get -y install postgresql-12-pg-stat-kcache
psql -d mydb -c "create extension if not exists pg_stat_kcache"

sudo apt-get -y install postgresql-12-pg-wait-sampling
psql -d mydb -c "create extension if not exists pg_wait_sampling"
```

- Adjust config:
```
shared_preload_libraries = pg_stat_statements,pg_wait_sampling,pg_stat_kcache
```

- Restart database

#### Instances

Copy and edit the `instances.yaml` configuration file (specify hosts you want to monitor):

```
sudo mkdir -p /etc/pgwatch2/config

sudo curl https://gitlab.com/postgres-ai/pgwatch2/-/raw/master/config/instances.yaml \
  --output /etc/pgwatch2/config/instances.yaml

sudo vim /etc/pgwatch2/config/instances.yaml
```

#### Docker

```bash
sudo docker run -d --name pgwatch2-postgresai \
  -p 3000:3000 -p 8081:8081 \
  -v /etc/pgwatch2/config:/etc/pgwatch2/config:ro \
  -v pgwatch2:/etc/pgwatch2/persistent-config \
  -v pgwatch2_postgres:/var/lib/postgresql \
  -v pgwatch2_grafana:/var/lib/grafana \
  -e PW2_GRAFANANOANONYMOUS=true \
  -e PW2_GRAFANAUSER="admin" \
  -e PW2_GRAFANAPASSWORD="MY_SECRET_PASS" \
  -e PW2_DATASTORE="postgres" \
  -e PW2_PG_SCHEMA_TYPE="timescale" \
  -e PW2_PG_RETENTION_DAYS=30 \
  -e PW2_TIMESCALE_CHUNK_HOURS=1 \
  -e PW2_TIMESCALE_COMPRESS_HOURS=1 \
  --restart=unless-stopped \
  --shm-size=1g \
  postgresai/pgwatch2:1.9.0-5
```

#### Dashboards

To view the dashboards, navigate to `http://<server-ip>:3000`


## Troubleshooting
Log into the container and look at log files - they’re situated under `/var/log/supervisor/`

Example:
```bash
sudo docker exec pgwatch2-postgresai ls -lh /var/log/supervisor/
sudo docker exec pgwatch2-postgresai tail -n 50 /var/log/supervisor/pgwatch2-stderr---supervisor-21oji6p3.log
```
